﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 퇴근알림이
{
    public partial class Form1 : Form
    {

        public static Form1 싱글톤이다;
        public Form1()
        {
            InitializeComponent();
        }


        DateTime 퇴근시간;
        private void Form1_Load(object sender, EventArgs e)
        {
            if (싱글톤이다 == null) 싱글톤이다 = this;
            퇴근시간 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Properties.Settings.Default.퇴근시간, Properties.Settings.Default.퇴근시간_분, 0);
            label1.Font = new Font(label1.Font.FontFamily, Properties.Settings.Default.폰트크기);
            timer1.Interval = Properties.Settings.Default.갱신주기;

            this.SetDesktopLocation(Screen.PrimaryScreen.Bounds.Right- this.Size.Width, Screen.PrimaryScreen.Bounds.Bottom - this.Size.Height - 35);
            시간설정();

            var 메뉴 = new ContextMenuStrip();
            메뉴.Items.Add("퇴근시간 설정",null,(오브젝트,이벤트아규먼트) =>
            {
                인풋박스.보기(인풋박스타입.퇴근시간설정, (i) =>
                 {
                     //소수점자리
                     Properties.Settings.Default.퇴근시간_분 = (int)(i - (int)i) * 60;
                     Properties.Settings.Default.퇴근시간 = (int)i;
                     퇴근시간 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Properties.Settings.Default.퇴근시간, Properties.Settings.Default.퇴근시간_분, 0);
                     Properties.Settings.Default.Save();
                 });
            });
            메뉴.Items.Add("갱신시간 설정", null, (오브젝트, 이벤트아규먼트) =>
            {
                인풋박스.보기(인풋박스타입.갱신주기설정, (i) =>
                {
                    Properties.Settings.Default.갱신주기 = (int)i;
                    timer1.Interval = Properties.Settings.Default.갱신주기;
                    Properties.Settings.Default.Save();
                });
            });
            메뉴.Items.Add("폰트크기 설정", null, (오브젝트, 이벤트아규먼트) =>
            {
                인풋박스.보기(인풋박스타입.폰트크기설정, (i) =>
                {
                    Properties.Settings.Default.폰트크기 = (int)i;
                    label1.Font = new Font(label1.Font.FontFamily, Properties.Settings.Default.폰트크기);
                    Properties.Settings.Default.Save();
                });
            });
            메뉴.Items.Add("폰트색깔 설정", null, (오브젝트, 이벤트아규먼트) =>
            {
                인풋박스.보기(인풋박스타입.폰트색깔설정, (i) =>
                {
                    ///콜백은 아래 폰트색설정(Color)
                });
            });
            메뉴.Items.Add("종료", null, (오브젝트, 이벤트아규먼트) =>
            {
                Application.Exit();
            });
            notifyIcon1.ContextMenuStrip = 메뉴;
        }


        public void 폰트색설정(Color col)
        {
            label1.ForeColor = col;
            Properties.Settings.Default.폰트색 = col;
            Properties.Settings.Default.Save();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            시간설정();
        }

        void 시간설정()
        {
            if ((퇴근시간 - DateTime.Now).TotalSeconds < 0)
            {
                label1.Text = $"퇴근 시간이 지났습니다. 퇴근하세요!";
            }
            else
            {
                label1.Text = $"퇴근 시간까지 {(퇴근시간 - DateTime.Now).TotalSeconds:0.00}초 남았습니다.";
            }
        }
    }
}
