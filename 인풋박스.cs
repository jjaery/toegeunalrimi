﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 퇴근알림이
{
    public enum 인풋박스타입
    {
        갱신주기설정,
        퇴근시간설정,
        폰트크기설정,
        폰트색깔설정
    }


    public partial class 인풋박스 : Form
    {
        
        Action<float> 등록콜백숫자;

        public static 인풋박스 보기(인풋박스타입 타입,Action<float> 콜백)
        {
            var 박스 = new 인풋박스(타입, 콜백);
            if (박스 == null) return null;
            박스.Show();
            return 박스;
        }


        인풋박스타입 객체의타입;
        public 인풋박스(인풋박스타입 타입, Action<float> 콜백)
        {
            InitializeComponent();

            단위.Visible = 타입 == 인풋박스타입.퇴근시간설정;
            textBox2.Visible = 타입 == 인풋박스타입.퇴근시간설정;
            label2.Visible = 타입 == 인풋박스타입.퇴근시간설정;

            switch (타입)
            {
                case 인풋박스타입.갱신주기설정:

                    label1.Text = "갱신주기를 입력하세요. (1000 = 1초)";
                    textBox1.Text = Properties.Settings.Default.갱신주기.ToString();
                    break;
                case 인풋박스타입.퇴근시간설정:
                    label1.Text = "퇴근시간을 입력하세요. (24시 - 24, 오후 7시 - 19)";
                    단위.Text = "시";
                    textBox1.Text = Properties.Settings.Default.퇴근시간.ToString();
                    textBox2.Text = Properties.Settings.Default.퇴근시간_분.ToString();
                    break;
                case 인풋박스타입.폰트크기설정:
                    label1.Text = "폰트 크기를 입력하세요. (pt)";
                    textBox2.Text = Properties.Settings.Default.폰트크기.ToString();
                    break;
                case 인풋박스타입.폰트색깔설정:
                    if (colorDialog1.ShowDialog() == DialogResult.OK)
                    {
                        Form1.싱글톤이다.폰트색설정(colorDialog1.Color);
                        this.Close();
                    }
                    else
                    {
                        this.Close();
                    }
                    break;
            }
            this.Text = 타입.ToString();
            객체의타입 = 타입;
            등록콜백숫자 = 콜백;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ///에러검사
            ///공용
            float 숫자;
            if (!float.TryParse(textBox1.Text, out 숫자))
            {
                MessageBox.Show("숫자를 입력하세요.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            switch (객체의타입)
            {
                case 인풋박스타입.퇴근시간설정:
                    if(숫자 >= 24 || 숫자 < 0)
                    {
                        MessageBox.Show("시는 0~23 까지 됩니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    float 분;
                    if (!float.TryParse(textBox2.Text, out 분))
                    {
                        MessageBox.Show("숫자를 입력하세요.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    if (분 >= 60 || 숫자 < 0)
                    {
                        MessageBox.Show("분은 0~60 까지 됩니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    숫자 += (분 / 60f);
                    break;
                case 인풋박스타입.갱신주기설정:
                case 인풋박스타입.폰트크기설정:
                    break;
            }

            등록콜백숫자(숫자);
            this.Close();
        }
        
    }
}
